const express = require('express');
const mongoose = require('../mongoose')
const transporter = require('../nodemailer');
const { Patient } = require('./patient');
const { Doctor } = require('./doctor');

router = express.Router();

const consultationSchema = new mongoose.Schema({
  date: {
    type: Date,
    required: true,
  },
  doctor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Doctor',
    required: true,
  },
  patient: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Patient',
    required: true,
  },
});

const Consultation = mongoose.model('Consultation', consultationSchema);

router.get('/consultations', async (req, res) => {
  try {
    const consultations = await Consultation.find()
      .populate('doctor')
      .populate('patient');
    res.send(consultations);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

router.get('/consultations/:id', async (req, res) => {
  try {
    const consultation = await Consultation.findById(req.params.id)
      .populate('doctor')
      .populate('patient');
    if (!consultation) {
      return res.status(404).send('Consultation not found');
    }
    res.send(consultation);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

router.post('/consultations', async (req, res) => {
  try {
    const { date, doctorId, patientId } = req.body;

    const doctor = await Doctor.findById(doctorId);
    if (!doctor) {
      return res.status(400).send('Invalid doctor id');
    }

    const patient = await Patient.findById(patientId);
    if (!patient) {
      return res.status(400).send('Invalid patient id');
    }

    const consultation = new Consultation({
      date,
      doctor: doctor._id,
      patient: patient._id,
    });

    await consultation.save();

    const mailOptions = {
      from: doctor.email,
      to: patient.email,
      subject: 'Nova consulta',
      text: `Olá ${patient.name}, você tem uma nova consulta com o Dr. ${doctor.name} em ${date}.`,
    };

    await transporter.sendMail(mailOptions);

    res.status(201).send(consultation);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

router.put('/consultations/:id', async (req, res) => {
  try {
    const { date, doctorId, patientId } = req.body;

    const doctor = await Doctor.findById(doctorId);
    if (!doctor) {
      return res.status(400).send('Invalid doctor id');
    }

    const patient = await Patient.findById(patientId);
    if (!patient) {
      return res.status(400).send('Invalid patient id');
    }

    const consultation = await Consultation.findByIdAndUpdate(
      req.params.id,
      {
        date,
        doctor: doctor._id,
        patient: patient._id,
      },
      { new: true }
    )
      .populate('doctor')
      .populate('patient');
    if (!consultation) {
      return res.status(404).send('Consultation not found');
    }

    res.send(consultation);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

router.delete('/consultations/:id', async (req, res) => {
  try {
    const consultation = await Consultation.findByIdAndDelete(req.params.id)
      .populate('doctor')
      .populate('patient');
    if (!consultation) {
      return res.status(404).send('Consultation not found');
    }

    res.send(consultation);
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});
