const express = require('express');
const mongoose = require('../mongoose')

router = express.Router();

const patientSchema = new mongoose.Schema({
  name: String,
  email: String,
  phone: String,
  birthdate: Date,
});

export const Patient = mongoose.model('Patient', patientSchema);

router.get('/patients', async (req, res) => {
  const patients = await Patient.find();
  res.json(patients);
});

router.get('/patients/:id', async (req, res) => {
  const id = req.params.id;
  const patient = await Patient.findById(id);
  if (patient) {
    res.json(patient);
  } else {
    res.sendStatus(404);
  }
});

router.post('/patients', async (req, res) => {
  const { name, email, phone, birthdate } = req.body;
  const newPatient = new Patient({ name, email, phone, birthdate });
  await newPatient.save();
  res.json(newPatient);
});

router.put('/patients/:id', async (req, res) => {
  const id = req.params.id;
  const patient = await Patient.findById(id);
  if (patient) {
    const { name, email, phone, birthdate } = req.body;
    patient.name = name;
    patient.email = email;
    patient.phone = phone;
    patient.birthdate = birthdate;
    await patient.save();
    res.json(patient);
  } else {
    res.sendStatus(404);
  }
});

router.delete('/patients/:id', async (req, res) => {
  const id = req.params.id;
  const patient = await Patient.findById(id);
  if (patient) {
    await patient.remove();
    res.sendStatus(204);
  } else {
    res.sendStatus(404);
  }
});
