const express = require('express');
const mongoose = require('../mongoose')

router = express.Router();

const doctorSchema = new mongoose.Schema({
  name: String,
  email: String,
  specialty: String,
});

export const Doctor = mongoose.model('Doctor', doctorSchema);

router.get('/doctors', async (req, res) => {
  const doctors = await Doctor.find();
  res.json(doctors);
});

router.get('/doctors/:id', async (req, res) => {
  const id = req.params.id;
  const Doctor = await Doctor.findById(id);
  if (Doctor) {
    res.json(Doctor);
  } else {
    res.sendStatus(404);
  }
});

router.post('/doctors', async (req, res) => {
  const { name, specialty } = req.body;
  const newDoctor = new Doctor({ name, specialty });
  await newDoctor.save();
  res.json(newDoctor);
});

router.put('/doctors/:id', async (req, res) => {
  const id = req.params.id;
  const Doctor = await Doctor.findById(id);
  if (Doctor) {
    const { name, specialty } = req.body;
    Doctor.name = name;
    Doctor.specialty = specialty;
    await Doctor.save();
    res.json(Doctor);
  } else {
    res.sendStatus(404);
  }
});

router.delete('/doctors/:id', async (req, res) => {
  const id = req.params.id;
  const Doctor = await Doctor.findById(id);
  if (Doctor) {
    await Doctor.remove();
    res.sendStatus(200);
  } else {
    res.sendStatus(404);
  }
});
