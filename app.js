const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const doctorsRouter = require('./routes/doctors');
const patientsRouter = require('./routes/patients');
const consultationsRouter = require('./routes/consultations');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('', indexRouter);
app.use('', doctorsRouter);
app.use('', patientsRouter);
app.use('', consultationsRouter);

module.exports = app;
